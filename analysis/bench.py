#
# Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
# Distributed under the GPL-3
#
# --------------------------------------------------------------------
def loadLog(filename, prefix=None):
    from numpy import asarray
    if prefix is not None and prefix != '':
        filename = prefix + '/' + filename
        
    with open(filename, 'r') as file:
        lines   = file.readlines()
        dataset = lines[0].strip('\n')
        test    = lines[1].strip('\n')
        version = lines[2].strip('\n')
        compiler= lines[3].strip('\n')
        data    = asarray([[int(l) for l in line.split()]
                           for line in lines[4:]])

    return test, version, data

# --------------------------------------------------------------------
def profile(value,n,bins):
    from numpy import digitize,asarray, sqrt
    
    binc = ((bins[1:]+bins[:-1])/2)
    binw = (bins[1:]-bins[:-1])
    nbin = digitize(n,bins)
    s    = asarray([len(value[nbin==bin])>0
                    for bin in range(len(bins)-1)])
    m    = asarray([value[nbin==bin].mean()
                    for bin in range(len(bins)-1) if s[bin]])
    e    = asarray([value[nbin==bin].std()/sqrt(len(value[nbin==bin]))  
                    for bin in range(len(bins)-1) if s[bin]])
    
    return binc[s],m,e,(binw[s]/2)  

# --------------------------------------------------------------------
def meanTime(data,bins):
    from numpy import isfinite
    t = data[:,1]# / data[:,0]
    x,y,ey,ex = profile(t,data[:,0],bins)
    m = isfinite(y)
    return x[m], y[m], ey[m], ex[m]

# --------------------------------------------------------------------
def ratioTimes(num,den,bins):
    t = num[:,1] / den[:,1]
    return profile(t,den[:,0],bins)

# --------------------------------------------------------------------
def fitIt(data,bins,f):
    from nbi_stat import fit, chi2nu
    t    = meanTime(data,   bins)
    m    = t[2] >= 0;
    tz   = t[0][m], t[1][m], t[2][m]
    p, c = fit(f,t[0], t[1], f.p0, t[2])
    chi2, nu = chi2nu(t[0], t[1], f, p, t[2])
    return t, p, c, chi2/nu

# --------------------------------------------------------------------
class LabelFormatter:
    def __init__(self,ax,label='',unit='',axis='y'):
        self._axis  = ax.yaxis if axis == 'y' else ax.xaxis
        self._label = label
        self._unit  = unit
        ax.callbacks.connect(axis+'lim_changed',self.update);
        ax.figure.canvas.draw()
        self.update(None)

    def update(self,lim):
        fmt = self._axis.get_major_formatter()
        self._axis.offsetText.set_visible(False)
        self._axis.set_label_text(self._label+' '+
                                  fmt.get_offset()+' '+
                                  self._unit)
        
# --------------------------------------------------------------------
def plotIt(data,bins,f,p,c,ax,**kwargs):
    from nbi_stat import fit_plot
    from matplotlib.ticker import ScalarFormatter

    color   = kwargs.pop('color','k');
    label   = kwargs.pop('label','');
    datakw  = {'label':label,
               'c':color,
               'ls':'none',
               'marker':'o'}
    fitkw   = {'c',color}
    tablekw = {'title':label,
               'color': color}
    datakw  .update(kwargs.pop('data',{}))
    fitkw   .update(kwargs.pop('fit',{}))
    tablekw .update(kwargs.pop('table',{}))

    #xeval = data[0][data[2]>=0]
    fit_plot(*data[:3], f, p,c, axes=ax,
             #xeval=xeval,
             data=datakw,
             fit=fitkw,
             parameters=f.parnames,
             table=tablekw,
             **kwargs)

    ax.set_xlabel(r'$N_{\mathrm{in}}$')
    ax.set_ylabel(r'$\langle t\rangle$')
    yfmt = ScalarFormatter(useMathText=True);
    yfmt.set_powerlimits((-3,2));
    ax.yaxis.set_major_formatter(yfmt);
    lo = LabelFormatter(ax, label=ax.get_ylabel(), unit='$(\mathrm{ns})$',axis='y')
    

# --------------------------------------------------------------------
def loadIt(testName,dataset,version, prefix=''):
    fname = \
        f'{testName}'+\
        f'_{version}'+\
        f'{"" if dataset is None or dataset=="" else "_"+dataset}'+\
        '.log'

    return loadLog(fname, prefix=prefix)

# --------------------------------------------------------------------
def doOne(testName,f,dataset='',bins=None,ax=None,prefix='..'):
    if ax is None:
        from matplotlib.pyplot import gca
        ax = gca()
    
    if bins is None:
        from numpy import linspace
        bins = linspace(0,3000,30)
        
    nmine,   vmine,   mine   = loadIt(testName,dataset,'mine',   prefix=prefix)
    nmaster, vmaster, master = loadIt(testName,dataset,'master', prefix=prefix)
    if nmine != testName or vmine != 'mine':
        raise RuntimeError(f'File "{testName}_mine.log" has '
                           f'test={nmine} and version={vmine} '
                           f'which is wrong')
    if nmaster != testName or vmaster != 'master':
        raise RuntimeError(f'File "{testName}_master.log" has '
                           f'test={nmaster} and version={vmaster} '
                           f'which is wrong') 
   

    tmine,  pmine,  cmine,   _ = fitIt(mine,   bins, f);
    tmaster,pmaster,cmaster, _ = fitIt(master, bins, f);

    plotIt(tmine,   bins, f, pmine,   cmine,   ax,
           label='Mine',   color='C0',
           table={'loc':'center right'});
    plotIt(tmaster, bins, f, pmaster, cmaster, ax,
           label='Master', color='C1',
           table={'loc':'lower right'});
    fname = testName.replace('_',r'\_')
    ax.legend(title=fr'$\mathbf{{{fname}}}$')
    ax.set_yscale('log')
    ax.text(.5,.95,f.expr,ha='center',va='top',
            transform=ax.transAxes)

# --------------------------------------------------------------------
def tryOne(testName,dataset='',bins=None,ax=None,funcs=None,prefix='..'):
    if ax is None:
        from matplotlib.pyplot import gca
        ax = gca()
    
    if bins is None:
        from numpy import linspace
        bins = linspace(0,3000,21)
        
    nmine,   vmine,   mine   = loadIt(testName,dataset,'mine',   prefix=prefix)
    nmaster, vmaster, master = loadIt(testName,dataset,'master', prefix=prefix)
    if nmine != testName or vmine != 'mine':
        raise RuntimeError(f'File "{testName}_mine.log" has '
                           f'test={nmine} and version={vmine} '
                           f'which is wrong')
    if nmaster != testName or vmaster != 'master':
        raise RuntimeError(f'File "{testName}_master.log" has '
                           f'test={nmaster} and version={vmaster} '
                           f'which is wrong') 

    if funcs is None:
        funcs   = [nplogn]+[make_polyn(n) for n in range(2,5)]
    bestf   = None
    bestp   = None
    bestc   = None
    bestx   = 1e9

    for f in funcs:
        try:
            tmine,  pmine,  cmine,   xmine   = fitIt(mine,   bins, f);
            tmaster,pmaster,cmaster, xmaster = fitIt(master, bins, f);

            x = min(xmine,xmaster)
            # print(f.__name__,x,pmine[-1],pmaster[-1])
            if x < bestx and (pmine[-1]>0 and pmaster[-1]>0):
                bestx = x
                bestf = f
                bestp = [pmine, pmaster]
                bestc = [cmine, cmaster]
        except:
            pass

    plotIt(tmine,   bins, bestf, bestp[0],  bestc[0], ax,
           label='Mine',   color='C0',
           table={'loc':'center right' if f == nplogn else 'center left'});
    plotIt(tmaster, bins, bestf, bestp[1],  bestc[1], ax,
           label='Master', color='C1',
           table={'loc':'lower right'});
    fname = testName.replace('_',r'\_')
    ax.legend(title=fr'$\mathbf{{{fname}}}$')
    if bestf == nplogn: ax.set_yscale('log')
    ax.text(.5,.95,bestf.expr,ha='center',va='top',
            transform=ax.transAxes)

    
# --------------------------------------------------------------------
def nplogn(n,*a):
    from numpy import log
    return a[0] * n ** a[1] * log(n)
nplogn.parnames = ['A','p']
nplogn.p0       = [70,1]
nplogn.expr     = r'$f(N)=AN^{p}\log{N}$'

# --------------------------------------------------------------------
def polyn(n,*a):
    from numpy import power, arange, asarray, newaxis
    p = asarray(a)
    m = len(p)
    return (power(n[:,newaxis],arange(m)[newaxis,:])*p).sum(axis=1)

# --------------------------------------------------------------------
def make_polyn(n):
    def ff(n,*a):
        return polyn(n,*a)
    
    ff.parnames = [f'p_{i}' for i in range(n)]
    ff.p0       = [1]+[0]*(n-1)
    ff.expr     = fr'$f(N)=\sum_{{i=0}}^{{{n-1}}}p_{{i}}N^{{i}}$'
    ff.__name__ = f'poly{n}'
    return ff
    
# --------------------------------------------------------------------
allTests = ['add_attr', 
            'add_part', 
            'add_tree',
            'cnt_anc', 
            'cnt_dec', 
            'new_delete', 
            'print', 
            'read', 
            'rem_part', 
            'rem_vtx', 
            'rotate', 
            'write']
testConfig = {'rem_part': {'f': [nplogn]},
              'rem_vtx':  {'f': [nplogn]},
              'read':     {'f': [nplogn]+[make_polyn(n) for n in range(3,5)]}}

# --------------------------------------------------------------------
def tryAll(dataset='',tests=allTests,config=testConfig,prefix='..',bins=None):
    from matplotlib.pyplot import subplots

    ncols = 3
    nrows = (len(tests)+2)//ncols 

    fig, ax = subplots(ncols=ncols,nrows=nrows,
                       sharex=True, figsize=(12,18),
                       gridspec_kw={'hspace':0})

    for t, a in zip(tests,ax.flatten()):
        # print(t)
        funcs = config.get(t,{}).get('f',None)
        if funcs and len(funcs) == 1:
            doOne(t,funcs[0],
                  dataset=dataset,
                  bins = bins,
                  ax = a,
                  prefix=prefix)
        else:
            tryOne(t,
                   ax = a,
                   dataset=dataset,
                   bins = bins,
                   funcs = funcs,
                   prefix=prefix)
    
    if ncols*nrows > len(tests):
        ax.flatten()[-1].set_axis_off()

    fig.suptitle(dataset)
    fig.tight_layout();

    if dataset is not None and len(dataset) > 0:
        fig.savefig(dataset+'.png')

    return fig
#
# EOF
#
