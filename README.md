# Benchmark of HepMC3 

The top-level [`Makefile`](Makefile) and the
[`eg/Makefile`](eg/Makefile) makes certain assumptions about where
HepMC3 and Pythia8 is installed. 

- `master` of HepMC3 is assumed to be installed in `../install-master` 
- `mine` (whatever that is) of HepMC3 is assumed to be installed in
  `../install-mine`. 
- Pythia8 is assumed to be installed in `/opt/sw/inst/` 

Change `HEPMC3_CONFIG` and `PYTHIA8_CONFIG` variable definitions to
suit your needs. 

## To make logs 

To read in 1000 Pythia8 pp events at $`\sqrt{s}=14\,\mathrm{TeV}`$
(Min.Bias.), do 

    > make logs master 
    > make logs mine 
    
The input file `all.hepmc` is automatically made from the sources in
[`eg`](eg). 

Assuming you have data set _dataset_ in sub-directory `inputs`, do

    > make dslogs master DATASET=dataset 
    > make dslogs mine   DATASET=dataset 
    
where `dataset` above is set to the name of the dataset (without
`.hepmc` or leading directory). 

## To make flamegraphs of profiling 

Do 

    > make read_mine.html 
    > make read_master.html 
    
to make HTML profiling reports of the `read` test.  Do similar for
other tests 

## Analysis 

The directory [`analysis`](analysis) contains Jupyter Notebooks to
analyse the logs. 

## Datasets 

Datasets can be retrieved from 

- [https://rivetval.web.cern.ch/rivetval/HEPMC/](https://rivetval.web.cern.ch/rivetval/HEPMC/) 

Most of these seem to be formatted in HepMC2 ASCII (aka `IO_GenEvent`)
format. 

Copyright (c) 2023 Christian Holm Christensen.  Distributed under the
[GPL-3](https://www.gnu.org/licenses/gpl-3.0.en.html). 


