//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver.hh>
#include <HepMC3/Relatives.h>

//--------------------------------------------------------------------
void cnt_anc(HepMC3::GenEvent& event)
{
  int n = 0;
  for (auto particle : event.particles())
    n += HepMC3::ancestor_particles(particle).size();
  n--;
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  
  return bench::driver(NAME(cnt_anc),cnt_anc,argc,argv);
}
//
// EOF
//


      
        
        
        
