//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver.hh>

void add_part(HepMC3::GenEvent& event)
{
  size_t n = 0;
  for (auto iter = event.vertices().rbegin();
       iter != event.vertices().rend(); ++iter) {
    if (n++ % 5 == 0) 
      (*iter)->add_particle_out(std::make_shared<HepMC3::GenParticle>());
  }
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{  
  return bench::driver(NAME(add_part),add_part,argc,argv);
}
//
// EOF
//


      
        
        
        
