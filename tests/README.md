# The concrete test 

Each source file defines a separate test as a stand-alone program. 

The tests are defined in terms of a single function that is called for
each event (except [`read.cc`](read.cc) and [`write.cc`](write.cc) -
more below) with a reference to the `HepMC3::GenEvent` object as the
sole argument. 

The test programs then uses the [`bench::driver`](../bench/driver.cc)
function to execute that test on each event by passing the test
function to the `bench::driver` function.  The calls are optionally
timed by a [`bench::timer`](../bench/timer.hh) and the result written
to [out put](../bench/logger.hh), together with the number of
particles in the event.

The single event test functions should do what ever operations on the
event needed.  The functions are executed by the function template
[bench::looper](../bench/driver.hh). 

## Read and write tests 

These two tests [`read.cc`](read.cc) and [`write.cc`](write.cc) does
not define a event specific test function.  Instead, the specialise
the [`bench::looper`](../bench/driver.hh) template function on
`nullptr`, and pass `nullptr` to
[`bench::driver`](../bench/driver.hh).  

- For [`read.cc`](read.cc) it is done so that we may time reading in
  an event. 
- For [`write.cc`](write.cc) it is done so we may declare the writer
  outside of the event loop. 
  
## Available tests 


- [`add_attr.cc`](add_attr.cc)
  Add attribute to all particles in event 
  
- [`add_part.cc`](add_part.cc)
  Add a particle to every 5th vertex in event 

- [`cnt_anc.cc`](cnt_anc.cc)
  Count ancestors of all particles in event 

- [`cnt_dec.cc`](cnt_anc.cc)
  Count descendants of all particles in event 

- [`new_delete.cc`](new_delete.cc)
  Allocate copy of event and deallocate it 
  
- [`print.cc`](print.cc)
  Print events 
  
- [`read.cc`](read.cc)
  Read in events 
  
- [`rem_part.cc`](rem_part.cc)
  Remove every 10th particle from the event 
  
  Note, this fails for `master` in certain input, but does not fail
  for `mine`
  
- [`rem_vtx.cc`](rem_vtx.cc)
  Remove every 10th vertex from the event 

  Note, this fails for `master` in certain input, but does not fail
  for `mine`

- [`rotate.cc`](rotate.cc) 
  Rotate event 
  
- [`write.cc`](write.cc)
  Write events.  If verbosity is 0, then the events are written to
  `/dev/null`, otherwise they are written to standard error. 
  
