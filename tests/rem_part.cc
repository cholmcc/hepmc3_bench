//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver.hh>
#include <list>

void rem_part(HepMC3::GenEvent& event)
{
  // This is two-staged, because if we remove elements while looping
  // over the list, then we can get problems.
  size_t n = 0;
  std::list<HepMC3::GenParticlePtr> toRemove;
  for (auto iter = event.particles().rbegin();
       iter != event.particles().rend(); ++iter) {
    if (n++ % 10 == 0) toRemove.emplace_back(*iter);
  }
  for (auto& p : toRemove) event.remove_particle(p);
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  return bench::driver(NAME(rem_part),rem_part,argc,argv);
}
//
// EOF
//


      
        
        
        
